//var general_var;

$(function(){
	
	/**
	 *  AppInfo Model
	 *  
	 *  传入app在itunes的网址, 获取app详细信息对象
	 *  var app_info = AppInfoModel({ url:app_url});
	 */
	var AppInfoModel = Backbone.Model.extend({
		defaults: {
			url: '~url~',
			name: '~name~'
		},
		initialize: function(){
			// 通过网址读取app信息，并放入对象属性
			this.remote_read( this.get('url') );
			
		},
		
		remote_read_url : '/api/app/get?id=',
		remote_read: function( app_id ){
			
			//_.bindAll(this, 'defaults');
			
			_this = this; // 解决ajax中失去this的问题
			
			$.ajax( {
					type: 'get',
					dataType:'json',
					url: _this.remote_read_url + app_id,
					//async:false, // 同步与异步
					beforeSend: function(){
						//alert('loading show');
						// Loading显示
					},
					complete:function(){
						//alert('loading close');
						// Loading 隐藏
					},
					success: function(json_data) {
					
						// 设置model数据
						_this.set( {
							name: json_data.app_name,
							intro: json_data.app_intro
						});
						
						//alert( data );
						alert( _this.get('name') );
					}
				}
				//_this.remote_read_url + app_id, function(data){
				
				//alert( _this.defaults );
				//alert( _this.get('url') );
				// 完成读取，设置属性

			);	
			
			
			
		}
		
	});
	
	/**
	 *	AppInfo  List 
	 */
	var AppInfoCollection = Backbone.Collection.extend({
		model: AppInfoModel
	});
	
	
	/**
	 *  Body View 整个页面
	 */
	var BodyView = Backbone.View.extend({
		el: $('body'),
		events: {
			'click button.button_get_app' : 'button_get_app_click'
		},
		initialize: function(){
			_.bindAll(this, 'button_get_app_click');
			
			// AppInfoCollection
			//this.app_info_collection = new AppInfoCollection;
			//this.app_info_collection.bind( 'add', this.app_info_list_view.append ); // 当数据collection增加时，视图也相应的增加app显示
			
		},
		
		button_get_app_click: function(){
			var app_info_model = new AppInfoModel({
				url : $('.input_app_id').val()
			});
			//alert( app_info_model.url() );
			
			//$('.input_app_id').
			//$.get('/', function(data){			
				// AppInfo model-collection 增加
				
				// AppInfoListView 增加 AppInfoView
				
				// 显示AppInfoListView

			//	alert(data);
			//});

			
			return false;
		}
		
	});
	
	/**
	 *	需要传入model
	 */
	var AppInfoView = Backbone.View.extend({
		tagName: 'li',
		
		
		initialize: function(){
			_.bindAll(this, 'render');
		},
		
		render: function(){
			$(this.el).html('<span>' + '</span>');
		}
	});

	
	
	/**
	 *	路由器
	 */
	var PageRouter = Backbone.Router.extend({
		initialize: function(){
			// 全个页面的视图
			var body_view = new BodyView;			
		},
		routes: {
			'*actions': 'defaultRoute'
		},
		defaultRoute:function(){		
			
		}
	});
	
	
	var page_router = new PageRouter();
	Backbone.history.start();
	
	// 按钮绑定
	//$('.button_get_app').click(function(){
	//	alert( $('.input_app_id').val() );
		
	//	return false;
	//});
});