#coding=utf-8

import web
from kk_lib.app_catcher import AppInfo
import json

urls = (
	'/', 'home',
	
	'/api/app/(.*)', 'api_app',  # 获取、抓去app信息
	
)

render = web.template.render('kk_templates/', base='base', cache=False)

web_app = web.application( urls, globals() )


class home:
	"""
	首页
	"""
	def GET(self):
		#web.header('Content-Type', 'text/json')
		return render.home()
		
		
class api_app:
	"""
	API :: 获取APP信息
	"""
	def GET(self, action):
		if action == 'get':
			# 获取app信息, 传入iTunes App 网址
			data = web.input()
			app_info = AppInfo( data.get('id') )
			
			return json.dumps( app_info.info, ensure_ascii=False )
		
		





if __name__ == '__main__':
	web_app.run()