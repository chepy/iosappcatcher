﻿#coding=utf-8
"""
	一个抓取iPhone App Store的类
	
	构造器传入网址，抓取app的详细信息
	
	
	
	@Author : Mrkelly
	@Contact : chepy.v@gmail.com,   18688180270
"""


APP_URL = "http://itunes.apple.com/cn/app//id471389306?mt=8"
APP_URL = "http://itunes.apple.com/cn/app//id353732572?mt=8" # DOUBAN

import urllib2
import sys
import BeautifulSoup

reload(sys)
sys.setdefaultencoding('utf-8')


class AppInfo:
	
	info = {}
	
	def __init__(self, app_url):
		app_html =  urllib2.urlopen( app_url ).read()
		app_html = app_html.decode('utf-8')
		
		# Soup对象
		app_soup = BeautifulSoup.BeautifulSoup( app_html,fromEncoding='utf-8' )
		
		# 原始的输入的网址
		self.info['app_url'] = app_url
		
		# 应用程序app的名称
		self.info['app_name'] = app_soup.find('div', {'id':'title'}).h1.string
		
		# app的详细介绍
		app_intro = app_soup.find('div', {'class':'product-review'}).p.contents
		# 以上包含<br />标签，将其转化成字符串
		str_app_intro = ''
		for c in app_intro:
			str_app_intro = '%s%s' % ( str_app_intro, c )
		# 详细介绍进行组合
		self.info['app_intro'] = str_app_intro
		
		# app类型
		self.info['app_genre'] = app_soup.find('li', {'class':'genre'}).a.string
		
		# app类型查看网址
		self.info['app_genre_url'] = app_soup.find('li', {'class':'genre'}).a['href']
		
		# app图标
		self.info['artwork_image'] = app_soup.find('div', {'class':'artwork'}).img['src']
		
		# 获取所有app截图的网址标签，即<img src="xxxxx />
		all_screenshots_tags = app_soup.findAll('img', {'class':'landscape'})
		self.info['app_screenshot'] = []
		
		for img_tag in all_screenshots_tags:
			self.info['app_screenshot'].append( img_tag['src'] )  # app截图集
		
		#self.info['app_screenshot'] = all_screenshots_tags 
	
	
	def __unicode__(self):
		return unicode(self.__str__)
	
	def __str__(self):
		return self.info.__str__() # 字典转成字符串

		







#app_soup.prettify()

# APP图标



if __name__ == '__main__':
	one_app = AppInfo( APP_URL )
	
	print one_app.info
	
	
	
	
	
	